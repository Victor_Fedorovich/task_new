
/*var t= [
    {
        id: 0,
        deadline: 1555687800 ,
        title: 'Помыть посуду',
        is_complete: false
    },
    {
        id: 1,
        deadline: 1555952400,
        title: 'Сходить в магазин',
        is_complete: true
    },
];
localStorage.setItem('task', JSON.stringify(t));*///сделать эти действия, сохранить, после запустить в браузере и затем можно удалить

var task = JSON.parse(localStorage.getItem('task'));
if(!task) {
    task=[];
}

$('#dates').daterangepicker({
    timePicker: true,
    singleDatePicker: true,
    timePicker24Hour: true,
    locale: {
        format: 'DD/MM/YYYY HH:mm'
      }
});

const window_in =document.getElementsByClassName('window_in')[0];
const modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
modalWrapper.addEventListener('click', hideDeleteModal);
document.getElementsByClassName('confirm-modal')[0].addEventListener('click', stopProp);
document.getElementsByClassName('add-modal')[0].addEventListener('click', stopProp)
document.getElementsByClassName('close-modal')[0].addEventListener('click', hideDeleteModal);
document.getElementsByClassName('close-modal')[1].addEventListener('click', hideDeleteModal);
document.getElementsByClassName('add-task')[0].addEventListener('click', showAddModal);
document.getElementById('submit').addEventListener('click', addTask);

const modalButtons = document.getElementsByClassName('item');
for(var i = 0; i<modalButtons.length; i++){
    modalButtons[i].addEventListener('click', hideDeleteModal);
}

document.getElementById('confirm').addEventListener('click', deleteTask);

var taskId;



renderAll();



const deleteCard = document.getElementsByClassName('card-delete');
for(var i = 0; i < deleteCard.length; i++) {
    deleteCard[i].addEventListener('click', showDeleteModal);
}



function renderTask(task) {
    window_in.innerHTML += ` <div class="card">
    <div class="task-info">
        <input class="checkbox" type="checkbox" ${task.is_complete ? 'checked' : ''}>
        <span>${convertToDate(task.deadline)}</span>
    </div>
<span class="task-name">${task.title}</span>                
 <div class="card-delete" id=${task.id}>X</div>
</div>`
}
function renderAll() {
    window_in.innerHTML = '';
    task.forEach(item => renderTask(item));
    //for( var i=0; i< tasl.length; i++) {
    //   renderTask}
}
function convertToDate(time) {
    moment.locale('ru');
    return moment.unix(time).format('lll');
}

function stopProp(ivent) {
    event.stopPropagation();
}

function showDeleteModal() {
    taskId=this.id;
    modalWrapper.style.display='flex';
    document.getElementsByClassName('confirm-modal')[0].style.display= 'flex';
    document.getElementsByClassName('add-modal')[0].style.display='none';
}
function showAddModal () {
    modalWrapper.style.display='flex';
    document.getElementsByClassName('confirm-modal')[0].style.display= 'none';
    document.getElementsByClassName('add-modal')[0].style.display='flex';
}

function hideDeleteModal(){
    modalWrapper.style.display='none';
}

function deleteTask() {
    const tasks = task.find(item => item.id === +taskId);
    const index = task.indexOf(tasks);
    task.splice(index, 1);
    renderAll();

    localStorage.setItem('task',JSON.stringify(task));

    const deleteCard = document.getElementsByClassName('card-delete');
for(var i = 0; i < deleteCard.length; i++) {
    deleteCard[i].addEventListener('click', showDeleteModal);
}
}


function addTask() {
    var drp = $('#dates').val();
   const unix = moment(drp, 'DD/MM/YYYY HH:mm').unix();//поиск времени через unix time
    
   var maxId=0;
    for(var i=0; i<task.length;i++) {
        if(maxId< task[i].id)
        maxId=task[i].id;
    }//находим макс айди

    const taskName = document.getElementById('title').value;//поиск названия задачи
    const tasks = {
        id: ++maxId,
        title: taskName,
        deadline: unix,
        is_complete: false
    };
    task.push(tasks);
    sortTasks();
    renderAll();
    localStorage.setItem('task',JSON.stringify(task));
    const deleteCard = document.getElementsByClassName('card-delete');
        for(var i = 0; i < deleteCard.length; i++) {
            deleteCard[i].addEventListener('click', showDeleteModal);
        }
    hideDeleteModal();
}

function sortTasks() {
    task.sort((a, b) => {
        return a.deadline - b.deadline;
    });
}








